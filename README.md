# dev on Weboob

## On docker
Install docker

download weboob devel source (https://git.weboob.org/weboob/devel/)

```bash
cd dev_weboob
docker build -t wod .
docker run -it -v ~/git/devel:/weboob --rm wod bash
```
Here you are inside the docker with the lastest python2.7 and all requirements installed

You can run:
```bash
python /devdir/weboob_exemple.py
```
You should get a BrowserIncorrectPassword. If not something wrong appends.

to dev you can edit into your host `~/git/weboob/modules` and relaunch weboob_example


## Prerequisite (without docker)


The following requirements are not necessairy if you run it on docker

### requirements (without docker)
On python2:

```
appdirs==1.4.3
atomicwrites==1.1.5
attrs==18.1.0
certifi==2018.4.16
chardet==3.0.4
cssselect==1.0.3
funcsigs==1.0.2
futures==3.2.0
html2text==2018.1.9
html5lib==1.0.1
idna==2.7
lxml==4.2.3
mechanize==0.3.6
more-itertools==4.2.0
olefile==0.45.1
Pillow==5.2.0
pluggy==0.6.0
prettytable==0.7.2
py==1.5.4
PySocks==1.6.8
python-dateutil==2.7.3
PyYAML==3.13
requests==2.19.1
six==1.11.0
Unidecode==1.0.22
urllib3==1.23
webencodings==0.5.1
xlrd==1.1.0

```

## Launch modules (without docker)
```bash
PYTHON_PATH=~/git/weboob python2
```
```python
import os
from weboob.core.modules import ModulesLoader
from weboob.core import Weboob
weboob = Weboob()
module_loader = ModulesLoader('~/git/weboob/modules', weboob.VERSION)
# abspath mandatory here
module_loader = ModulesLoader('/home/beranger/git/weboob/modules', weboob.VERSION)
module = module_loader.get_or_load_module('boursorama')
# additional creds can be used (see module to know there names)
creds = {'login': '1234', 'password': 'pw'}
instance = module.create_instance(weboob, 'boursorama', creds, storage=None)
# after the next line you are logged in or it will raise a BrowserIncorrectPassword
instance.iter_accounts()
acc = instance.get_account('id_of_account_given_by_iter_account')
for tr in instance.iter_history():
    print(tr.date, tr.label, tr.amount)
    
```

You can try with false creds it should raise a BrowserIncorrectPassword

