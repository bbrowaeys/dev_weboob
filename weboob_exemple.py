import os
from weboob.core.modules import ModulesLoader
from weboob.core import Weboob

def load_weboob_module(module_name, bank_name=None, creds={'login': '1234', 'password': 'pw'}):
    if not bank_name:
        bank_name
    weboob = Weboob()
    # abspath mandatory here
    module_loader = ModulesLoader('/weboob/modules', weboob.VERSION)
    module = module_loader.get_or_load_module(module_name)
    # additional creds can be used (see module to know there names)
    return module.create_instance(weboob, module_name, creds, storage=None)


if __name__ == '__main__':
    inst = load_weboob_module('boursorama')
    inst.iter_accounts()