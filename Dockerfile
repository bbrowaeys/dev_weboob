# Use an official Python runtime as a base image
FROM python:2.7-slim

ADD ./requirements.txt /requirements.txt

RUN pip install -r /requirements.txt
RUN mkdir /devdir

ADD ./*.py /devdir/

# Define environment variables
ENV PYTHONPATH=/weboob:/weboob/modules:/devdir
